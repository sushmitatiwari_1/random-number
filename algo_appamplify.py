import time


class BiasedRandom()
    lessCount = 0
    moreCount = 0

    def getRandom()
        res = int(time.time() % 10)
        while res == 5:
            res = int(time.time() % 10)
        if res > 5 and moreCount >= 73:
            while res >= 5:
                res = int(time.time() % 10)
        if res < 5 and  lessCount >= 27:
            while res <= 5:
                res = int(time.time() % 10)
        if res > 5:
            moreCount += 1
        else:
            lessCount += 1
        if moreCount == 73 and lessCount == 27:
            moreCount = 0
            lessCount = 0
        return res
